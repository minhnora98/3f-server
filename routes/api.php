<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$apiRoutes = function () {
    Route::post('signup', 'AuthenticationController@register');
    Route::post('login', 'AuthenticationController@login');
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('test', 'ManageApiController@test');
        Route::get('isofh/services/pacs/GetOrder/{id}', 'ManageApiController@getOrder');

    });
};

Route::group(['domain' => config('app.domain')], $apiRoutes);