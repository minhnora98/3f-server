<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ManageApiController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function test(Request $request)
    {
        // dd($this->user);
        $user = JWTAuth::toUser($request->token);        
        return response()->json(['result' => $user]);
    }

    public function getOrder($id, Request $request)
    {
        switch($id){
            case 1: return [
                "comment" => null,
                "Hl7Dat" => "MSH|^~&|HIS|ISOFH|PACS|PACSCOMPANY|20180403101104.229+0700||OMI^O23^OMI_O23|1023456|P|2.7|||AL|NE|VNM|UNICODE UTF-8PID|1||2143569^1611000309||TRẦN HẬU QUÝ||19360101|M|||03, ngõ 61, đườngtrung tiết , Thạch Quý, Thành phố Hà Tĩnh, Hà Tĩnh||^0906187456PV1|1|I|^P02^G20^^^^^^02-Khoa Hồi sức tích cực||||ltienhstc^Lê VinhTiến||||||||||||214356IN1||HT2424216297915|BHYTORC|NW|1023456|R02^Khoa Hồi sức tíchcực||CM||||||||||20180403101104.28+0700||||||||||||20180402000000+0700||ITQ1||1|Once||||20180402000000+0700||ROBR|1|1135172|R02^Khoa Hồi sức tích cực|010085^Chụp cắt lớp vi tính lồng ngực(16-32 dãy) không tiêm thuốc cản quang|||20180403101104.28+0700||||||Chẩn đoán:Suy hô hấp|||ltienhstc^Lê Vinh Tiến||02-Khoa Hồi sức tích cực||08-Khoa Chẩn đoánhình ảnh||||CT|||1|||PORTOBX|1|TX|^HEIGHT||90|cmOBX|2|TX|^WEIGHT||13|kg"
            ];
            case 2: return [
                "comment" => "Số phiếu không tồn tại trên HIS (Số phiếu RIS – PACS GET). Vui lòng kiểm tra lại!",
                "Hl7Dat" => "null"
            ];
            case 3: return [
                "comment" => "Tồn tại dịch vụ chưa được Thanh toán/ duyệt BH (Liệt kê các dịch vụ không thỏa mãn). Vui lòng kiểm tra lại!",
                "Hl7Dat" => null
            ];
        }
        
    }
}
